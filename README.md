# Titanic Webapp

## Introduction

The frontend webapp of a continuing project series using the titanic kaggle dataset. Full details of the project can be found at https://kennylouie.gitlab.io.

This webapp is built with reactjs and provides a frontend form that makes a POST request to a Go API built previously (gitlab.com/kennylouie/titanic-api).

A live demo can be found at https://kennylouie.gitlab.io/titanic-webapp. *Note* not currently capable for performing POST request due to having only a self-signed certificate on my AWS EC2 server.

## Dependencies

+ npm 8.11.3
+ yarn 1.9.2

## Running

``` bash
git clone https://gitlab.com/kennylouie/titanic-webapp.git
cd titanic-webapp
yarn
yarn start # for development server
yarn build # not yet suitable for production
```
