import React, { Component } from 'react';
import PredictForm from './PredictForm'
import PredictResult from './PredictResult'
import { Grid, Container, Dimmer, Loader } from 'semantic-ui-react'


class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      prediction: "SUBMIT A REQUEST",
      loading: false,
    }
  }

  predictionCallback = (childPrediction) => {
    this.setState({prediction: childPrediction})
  }

  loadingCallback = (childLoading) => {
    this.setState({loading: childLoading})
  }

  render() {
    return(
    <div>
      <Container>
        <Dimmer active={this.state.loading}>
          <Loader />
        </Dimmer>
        <Grid>
          <Grid.Row>
            <Grid.Column width={1}>
            </Grid.Column>
            <Grid.Column width={14}>
               <PredictForm predictionCallback={this.predictionCallback} loadingCallback={this.loadingCallback} />
            </Grid.Column>
            <Grid.Column width={1}>
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column width={6}>
            </Grid.Column>
            <Grid.Column width={4}>
               <PredictResult prediction={this.state.prediction} />
            </Grid.Column>
            <Grid.Column width={6}>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </div>
    )
  }
}

export default Home
