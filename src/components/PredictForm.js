import React, { Component } from 'react'
import { Button, Header, Container, Form } from 'semantic-ui-react'
import axios from 'axios'

const style = {
  h1: {
    marginTop: '3em',
    margin: '4em 0em 2em',
  },
}

class PredictForm extends Component {

  constructor(props) {
    super(props)
    this.state = {
      PassengerId: '',
      Age: '',
      Sex: '',
      Parch: '',
      Fare: '',
      Pclass: '',
      SibSp: '',
      Embarked: '',
      Prediction: '',
      Loading: true,
    }
  }

  handleChangePassengerId = event => {
    this.setState({ PassengerId: event.target.value })
  }
  handleChangeAge = event => {
    this.setState({ Age: event.target.value })
  }
  handleChangeSex = event => {
    this.setState({ Sex: event.target.value })
  }
  handleChangeParch = event => {
    this.setState({ Parch: event.target.value })
  }
  handleChangeFare = event => {
    this.setState({ Fare: event.target.value })
  }
  handleChangePclass = event => {
    this.setState({ Pclass: event.target.value })
  }
  handleChangeSibSp = event => {
    this.setState({ SibSp: event.target.value })
  }
  handleChangeEmbarked = event => {
    this.setState({ Embarked: event.target.value })
  }

  onPrediction = () => {
    var childPrediction = this.state.Prediction
    this.props.predictionCallback(childPrediction)
  }

  onLoading = () => {
    var childLoading = this.state.Loading
    this.props.loadingCallback(childLoading)
  }

  handleSubmit = event => {
    event.preventDefault()
    this.onLoading()

    axios.post('https://9nfmilz74g.execute-api.us-west-2.amazonaws.com/Prod/',
      JSON.stringify({
        PassengerId: this.state.PassengerId,
        Age: this.state.Age,
        Sex: this.state.Sex,
        Parch: this.state.Parch,
        Fare: this.state.Fare,
        Pclass: this.state.Pclass,
        SibSp: this.state.SibSp,
        Embarked: this.state.Embarked,
      }),
      { 'Content-Type': 'application/json' }
    )
      .then(prediction => {
        this.setState({Prediction: prediction.data.body})
        this.onPrediction()
        this.setState({Loading: false})
        this.onLoading()
      })

    this.setState({
      PassengerId: '',
      Age: '',
      Sex: '',
      Parch: '',
      Fare: '',
      Pclass: '',
      SibSp: '',
      Embarked: '',
    })

  }

  render() {
    return (
      <div>
        <Header as='h1' content='PREDICT A TITANIC SURVIVOR' style={style.h1} textAlign='center' />
        <Container>
          <Form onSubmit={this.handleSubmit}>
            <Form.Group widths='equal'>
              <Form.Input fluid label='PassengerId' placeholder='e.g. 3' value={this.state.PassengerId} required onChange={this.handleChangePassengerId}/>
              <Form.Input fluid label='Age' placeholder='e.g. 26' value={this.state.Age} required onChange={this.handleChangeAge}/>
              <Form.Input fluid label='Sex' placeholder='female/male' value={this.state.Sex} required onChange={this.handleChangeSex}/>
              <Form.Input fluid label='Parch' placeholder='e.g. 3' value={this.state.Parch} required onChange={this.handleChangeParch}/>
              <Form.Input fluid label='Fare' placeholder='e.g. 7.925' value={this.state.Fare} required onChange={this.handleChangeFare}/>
              <Form.Input fluid label='Pclass' placeholder='e.g. 3' value={this.state.Pclass} required onChange={this.handleChangePclass}/>
              <Form.Input fluid label='SibSp' placeholder='e.g. 0' value={this.state.SibSp} required onChange={this.handleChangeSibSp}/>
              <Form.Input fluid label='Embarked' placeholder='e.g. S' value={this.state.Embarked} required onChange={this.handleChangeEmbarked}/>
            </Form.Group>
            <Form.Group>
              <Form.Field
                id='form-button-control-public'
                control={Button}
                content='Submit'
              >
              </Form.Field>
            </Form.Group>
          </Form>
        </Container>
      </div>
    )
  }
}

export default PredictForm
