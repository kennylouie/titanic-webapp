import React, { Component } from 'react'
import { Statistic } from 'semantic-ui-react'

class PredictResult extends Component {

  render() {
    return(
      <div>
        <Statistic>
          <Statistic.Value>{this.props.prediction}</Statistic.Value>
          <Statistic.Label>Prediction</Statistic.Label>
        </Statistic>
      </div>
    )
  }

}

export default PredictResult
